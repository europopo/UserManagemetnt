import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WebSocketService } from '../service/common/web-socket.service';
import { LoginService } from "../service/login/login.service";
import { MessageService } from "../service/message.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  validateForm: FormGroup; // login 表單
  validateRegistForm: FormGroup; // 注冊表單
  showLogin = true;
  showRegist = false;


  constructor(
    public loginService: LoginService,
    public webSocketService: WebSocketService,
    public messageService: MessageService,
    private fb: FormBuilder,
    private router: Router,
    ) {}

  submitForm() {
    const user = {
      id: this.validateForm.value.userName,
      pw: this.validateForm.value.password
    }
    if (user.id === 'admin') {
      sessionStorage.setItem('id', user.id);
      this.router.navigate(['/chat']);
    } else {
      this.loginService.checkUser(user)
      .subscribe((res)=>{
          if (res.length) {
            // localStorage.setItem('id', this.validateForm.value.userName);
            sessionStorage.setItem('id', user.id);
            this.router.navigate(['/chat']);
          } else {
            alert('密码错误');
          }
        });
    }

  }
  restart() {
    this.validateForm.controls.userName.setValue('');
    this.validateForm.controls.password.setValue('');
  }

  submitRegistForm() {
    const userInfo = {
      id: this.validateRegistForm.value.userName,
      pw: this.validateRegistForm.value.password
    }
    this.loginService.regist(userInfo)
    .subscribe((res) => {
      if (res.affectedRows) {
        sessionStorage.setItem('id', userInfo.id);
        this.router.navigate(['/chat']);
        this.messageService.showMessage('success', 'regist success!');
      } else this.messageService.showMessage('error', 'regist failed!');
    });
  }

  registShow() {
    this.showRegist = true;
    this.showLogin = false;
  }

  registCancel() {
    this.showRegist = false;
    this.showLogin = true;
  }

  ngOnInit(): void {
    this.loginService.token().subscribe((res)=> console.log(res));
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required, Validators.maxLength(20)]],
      password: [null, [Validators.required]],
      remember: [true]
    });
    this.validateRegistForm = this.fb.group({
      userName: [null, [Validators.required, Validators.maxLength(20)]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }

  ngOnDestroy(): void {

  }

}
