import { Injectable } from '@angular/core';
import * as rrweb from "rrweb";
@Injectable({
  providedIn: 'root'
})

export class RrwebService {
  eventsArr: Array<Array<any>> = [[]];
  constructor() { }

  RecordRrweb() {
    rrweb.record({
      emit: (event: any, isCheckout: boolean) => {
        if (isCheckout) {
          if (this.eventsArr.length > 1) {
            this.eventsArr.shift();
          }
          this.eventsArr.push([]);
        }
        this.eventsArr[this.eventsArr.length - 1].push(event);
      },
      checkoutEveryNms: 30 * 1000, // 每30s重新制作快照
    });
  }

}
