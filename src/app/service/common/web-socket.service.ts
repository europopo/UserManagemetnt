import { Injectable } from '@angular/core';
import io from 'socket.io-client';
import { MessageService } from "../message.service";
// const datasource = require('../../../assets/config/datasource.json');
import {environment} from '../../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  ws: any;
  wsUrl = environment.socket;
  constructor(
    public messageService: MessageService,
  ) {
   }

  connectSocket() {
    this.ws = io(this.wsUrl, {autoConnect: true});
    this.ws.on('connect', ()=>{
      console.log('the Socket is connected!');
    });

    this.ws.on("disconnect", (reason) => {
      console.log("io disconnect reason:", reason);
      if (reason === "io server disconnect") {
        // the disconnection was initiated by the server, you need to reconnect manually
        this.reConnect();
      }
      this.reConnect();
      // else the socket will automatically try to reconnect
    });

    this.ws.on('show_Message', (msg) =>{
      this.messageService.showMessage('info', msg);
    });
  }

  emitSocketServer(event, data) {
    this.reConnect();
    this.ws.emit(event, data);
  }

  diconnectSocket() {
    this.ws.disconnect();
  }

  reConnect() {
    if (!this.ws.connected) {
      this.ws.connect();
      this.reJoin();
    }
  }

  reJoin() {
    if (sessionStorage.roomid) {
      this.ws.emit('join', {
        roomid: sessionStorage.roomid,
        id: sessionStorage.id,
      });
    }
  }
}
