import { HttpErrorResponse } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MessageService } from "../message.service";


// import db from "../../../assets/config/datasource.json";
// const datasource = require('../../../assets/config/datasource.json');
import {environment} from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  ROOTURL = environment.host;

  constructor(
    public http: HttpClient,
    public messageService: MessageService,
  ) {
  }


  get(url: string) {
    return this.http.get(this.ROOTURL + url);
  }

  post(url: string, body: any, options?: any) {
    return this.http.post(this.ROOTURL + url, body, options);
  }


}
