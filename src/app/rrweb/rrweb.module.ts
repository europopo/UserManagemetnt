import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RrwebRoutingModule } from './rrweb-routing.module';
import { RrwebComponent } from './rrweb.component';
import { NgZorroAntdModule } from '../tools/components/ng-zorro-antd.module';

@NgModule({
  declarations: [RrwebComponent],
  imports: [
    CommonModule,
    RrwebRoutingModule,
    NgZorroAntdModule
  ]
})
export class RrwebModule { }
