import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RrwebComponent } from './rrweb.component';

const routes: Routes = [{ path: '', component: RrwebComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RrwebRoutingModule { }
