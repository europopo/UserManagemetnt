import { Renderer2 } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import rrwebPlayer  from "rrweb-player";
import { HttpService } from "../service/common/http.service";
import { RrwebService } from "../service/rrweb.service";
@Component({
  selector: 'app-rrweb',
  templateUrl: './rrweb.component.html',
  styleUrls: ['./rrweb.component.css', '../../../node_modules/rrweb-player/dist/style.css']
})
export class RrwebComponent implements OnInit {


  action_btn: boolean = false;
  constructor(
    private render2: Renderer2,
    public http: HttpService,
    public rrwebService: RrwebService,
  ) { }

  ngOnInit(): void {
    this.rrwebService.RecordRrweb();
  }

  play() {
    // this.http.post('saverrwebevents', JSON.stringify(this.eventsArr.flat()), {
    //   headers: {'Content-Type': 'application/json',}
    // })
    // .subscribe((res)=>{
    //   console.log(res);
    // });
    console.log(this.rrwebService.eventsArr.length);

    this.http.http.get('../../assets/event_1659427953571').subscribe((res: any)=>{
      const p = new rrwebPlayer({
        target: this.render2.selectRootElement('.rrwebplayer'), // 可以自定义 DOM 元素
        // 配置项
        props: {
          events: res,
          // showWarning: false,
        },
      });
    })

    // this.action_btn = true;
    // const replayer = new rrweb.Replayer(this.eventsArr.flat(), {
    //   showWarning: false,
    // });
    // // // 播放
    // replayer.play();




  }

}
