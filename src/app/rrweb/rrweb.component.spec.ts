import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RrwebComponent } from './rrweb.component';

describe('RrwebComponent', () => {
  let component: RrwebComponent;
  let fixture: ComponentFixture<RrwebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RrwebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RrwebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
