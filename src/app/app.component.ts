import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit {
  constructor(
    ) {}

  ngOnInit() {
    console.log(environment.env);

  }

  ngOnDestroy() {


  }

}
