import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileSysComponent } from './file-sys.component';

describe('FileSysComponent', () => {
  let component: FileSysComponent;
  let fixture: ComponentFixture<FileSysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileSysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileSysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
