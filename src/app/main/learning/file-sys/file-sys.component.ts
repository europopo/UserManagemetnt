import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/service/common/http.service';

@Component({
  selector: 'app-file-sys',
  templateUrl: './file-sys.component.html',
  styleUrls: ['./file-sys.component.css']
})
export class FileSysComponent implements OnInit {

  constructor(
    public http: HttpService,
  ) { }

  ngOnInit(): void {
  }

  // uploadfile(e) {
  //   var file = e.files[0];
  //   var chunkSize = 1024 * 512; //每片1M大小
  //   var totalSize = file.size;
  //   var chunkQuantity = Math.ceil(totalSize/chunkSize); //分片总数
  //   var offset = 0; //偏移量

  //   var reader = new FileReader();
  //   reader.onload = (e) => {
  //     this.http.post('file', e.target.result)
  //     .subscribe((res)=>{
  //       if(res) {
  //         ++offset;
  //         if(offset === chunkQuantity) {
  //           alert("上传完成");
  //         } else if(offset === chunkQuantity-1) {
  //           blob = file.slice(offset*chunkSize, totalSize);
  //           reader.readAsArrayBuffer(blob);
  //         } else {
  //           blob = file.slice(offset*chunkSize, (offset+1)*chunkSize);
  //           reader.readAsArrayBuffer(blob);
  //         }
  //       } else {
  //         alert("上传出错")
  //       }
  //     });


  //     // xhr.onreadstatechange = function() {
  //     //   if(xhr.readyState === 4 && xhr.status ===200) {
  //     //     ++offset;
  //     //     if(offset === chunkQuantity) {
  //     //       alerrt("上传完成");
  //     //     } else if(offset === chunkQuantity-1) {
  //     //       blob = file.slice(offset*chunkSize, totalSize);
  //     //       reader.readAsBinaryString(blob);
  //     //     } else {
  //     //       blob = file.slice(offset*chunkSize, (offset+1)*chunckSize);
  //     //       reader.readAsBinaryString(blob);
  //     //     }
  //     //   }else {
  //     //     alert("上传出错")；
  //     //   }
  //     // }


  //     // if(xhr.sendAsBinary) {
  //     //   xhr.sendAsBinary(e.target.result);
  //     // } else {
  //     //   xhr.send(e.target.result);
  //     // }
  //   }
  //   var blob = file.slice(0, chunkSize);
  //   reader.readAsArrayBuffer(blob);
  // }

  uploadfile(e) {
    this.http.post('file', new Blob(['123']))
    .subscribe((res)=>{

    });


  }

  sendfile() {
    const file = new Blob(['123456']);
    this.http.post('file', {file: file})
    .subscribe((res)=> {
      console.log(res);
    });
  }
}
