import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/service/common/http.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    public http: HttpService,
  ) { }

  createRoom(roomid: string): Observable<any> {
    return this.http.post('room/create', {roomid: roomid});
  }

  joinRoom(roomid: string): Observable<any> {
    return this.http.post('room/join', {roomid: roomid});
  }

}
