import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, DoCheck } from '@angular/core';
import { WebSocketService } from '../service/common/web-socket.service';
import { MessageService } from '../service/message.service';
import { ChatService } from './service/chat.service';
import { LoginService } from "../service/login/login.service";
interface ChatData {
  id: string;
  roomid: string;
  num: number;
  content: string;
  datetime: Date;
}
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})

export class ChatComponent implements OnInit, OnDestroy, DoCheck {
  shownameinput = true; // 房間輸入框顯示
  showRoom = false;
  roomid: string = sessionStorage.roomid; // 房间名
  showCreateBtn = true; // create、join按鈕顯示
  chatMessages = []; // 讯息数据
  message: string; // input 键入的讯息
  nums = 0; // 房间内的人数
  colorList = ['#f56a00', '#7265e6', '#ffbf00', '#00a2ae'];
  color: string;
  id = sessionStorage.id; // 用戶id
  roomlist = [];
  visible: boolean = false;
  pageSize = 6;

  constructor(
    public chatService: ChatService,
    public messageService: MessageService,
    public webSocketService: WebSocketService,
    public loginService: LoginService
  ) { }




  ngOnInit(): void {
    this.webSocketService.connectSocket();
    this.webSocketService.reJoin();
    this.webSocketService.emitSocketServer('roomlist', {});
    this.webSocketService.ws.on('chat_join', (data: ChatData) => {
      this.messageService.showMessage('info', data.id + '进入房间');
      this.nums = data.num;
      this.color = this.colorList[this.nums % this.colorList.length];
      this.shownameinput = false;
      this.showRoom = true;
    });

    this.webSocketService.ws.on('chat_leave', (data: ChatData) => {
      this.messageService.showMessage('info', data.id + '离开房间');
      this.nums = data.num;
    });

    this.webSocketService.ws.on('chat_addMessage', (data: ChatData) => {
      this.chatMessages = [...this.chatMessages, data];
    });

    this.webSocketService.ws.on('chat_roomlist', (data: Array<any>) => {
      this.roomlist = data;
    });

  }

  ngOnDestroy() {
    this.leaveRoom();

    this.webSocketService.diconnectSocket();
  }

  ngDoCheck() {
    const el = document.getElementsByClassName('message-borad')[0];
    if (el) {
      el.scrollTop = el.scrollHeight;
    }
  }


  leaveRoom() {
    this.webSocketService.ws.emit('leave', {roomid: this.roomid, id: this.id});
    sessionStorage.removeItem('roomid');
    this.showRoom = false;
    this.shownameinput = true;
  }

  joinRoom(roomid?: string) {
    this.roomid = roomid || this.roomid;
    if (this.roomid) {
      sessionStorage.roomid = this.roomid;
      this.chatMessages = [];
      this.webSocketService.emitSocketServer('join', {roomid: this.roomid, id: this.id});
    }
  }

  showMessage() {
    if (this.message) {
      this.webSocketService.emitSocketServer('addmessage', {roomid: this.roomid, content: this.message.trim(), id: this.id, color: this.color});
      this.message = '';
    }
  }

  // disconnect() {
  //   this.webSocketService.diconnectSocket();
  // }

}
