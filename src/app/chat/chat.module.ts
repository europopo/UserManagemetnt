import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './chat.component';
import { NgZorroAntdModule } from '../tools/components/ng-zorro-antd.module';
import {ScrollingModule} from '@angular/cdk/scrolling';

@NgModule({
  declarations: [ChatComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChatRoutingModule,
    NgZorroAntdModule,
    ScrollingModule,
  ]
})
export class ChatModule { }
